#!/bin/bash
################################################################################
# Copyright 2024 ModalAI Inc.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# 4. The Software is used solely in conjunction with devices provided by
#    ModalAI Inc.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
################################################################################


export SKU_FILENAME="/data/modalai/sku.txt"

## OLD DEPRECATED DO NOT USE
export FACTORY_FILENAME="/data/modalai/factory_mode.txt"

################################################################################
# FAMILY NAMES
################################################################################

# WARNING: DO NOT UPDATE THIS LIST WITHOUT ALSO UPDATING $VOXL_FAMILY_NAMES in the same order
export VOXL_FAMILY_CODES=(
	"MRB-D0014"
	"MRB-D0012"
	"MRB-D0008"
	"MRB-D0013"
	"MRB-D0016"
	"MRB-D0005"
	"MRB-D0011"
	"MRB-D0006"
	"MRB-D0010"
	"MRB-D0015"
	"MRB-D0001"
	"MCM-C0001"
	"MRB-D0003"
	"MRB-D0004"
	"MDK-F0001"
	"MDK-F0002"
	"MDK-F0006"
	"TF-M0054"
	"TF-M0104"
	"MCCA-M0054"
	"MCCA-M0104"
	"MVX-T0001"
	"MVX-R0001")

# WARNING: DO NOT UPDATE THIS LIST WITHOUT ALSO UPDATING $VOXL_FAMILY_CODES in the same order
export VOXL_FAMILY_NAMES=(
	"starling-2"
	"starling-2-max"
	"stalker-vision"
	"stinger"
	"sparrow"
	"starling-1-discontinued"
	"px4-autonomy-dev-kit"
	"sentinel"
	"D0010"
	"D0015"
	"m500"
	"voxlcam"
	"seeker"
	"rb5-flight"
	"flight-deck"
	"voxl-deck"
	"voxl2-flight-deck"
	"voxl2-test-fixture"
	"voxl2-mini-test-fixture"
	"voxl2-board-only"
	"voxl2-mini-board-only"
	"MVX-T0001"
	"MVX-R0001")

export NUM_VOXL_FAMILIES="${#VOXL_FAMILY_CODES[@]}"

voxl-print-family-table (){

	for i in `seq 1 $NUM_VOXL_FAMILIES`; do
		echo "${VOXL_FAMILY_CODES[$i-1]} ${VOXL_FAMILY_NAMES[$i-1]}"
	done
}

voxl-family-code-to-name (){

	if [ "$#" -ne 1 ]; then
		>&2 echo "ERROR in voxl-family-code-to-name, expected 1 argument"
		return
	fi

	for i in `seq 1 $NUM_VOXL_FAMILIES`; do
		if [[ "${VOXL_FAMILY_CODES[$i-1]}" = "${1}" ]]; then
			echo "${VOXL_FAMILY_NAMES[$i-1]}"
			return
		fi
	done

	echo "ERROR in voxl-family-code-to-name, unknown family code ${1}"
	echo "valid codes: ${VOXL_FAMILY_CODES[@]}"
}

voxl-family-name-to-code (){

	if [ "$#" -ne 1 ]; then
		>&2 echo "ERROR in voxl-family-name-to-code, expected 1 argument"
		return
	fi

	for i in `seq 1 $NUM_VOXL_FAMILIES`; do
		if [[ "${VOXL_FAMILY_NAMES[$i-1]}" = "${1}" ]]; then
			echo "${VOXL_FAMILY_CODES[$i-1]}"
			return
		fi
	done

	echo "ERROR in voxl-family-name-to-code, unknown family name ${1}"
	echo "valid codes: ${VOXL_FAMILY_CODES[@]}"
}

voxl-is-valid-family-code (){
	if [ "$#" -ne 1 ]; then
		>&2 echo "ERROR in voxl-is-valid-family-code, expected 1 argument"
		return 1
	fi
	for i in ${VOXL_FAMILY_CODES[@]}; do
			[[ "$i" = "$1" ]] && return 0
	done
	return 1
}

voxl-is-valid-family-name(){
	if [ "$#" -ne 1 ]; then
		>&2 echo "ERROR in voxl-is-valid-family_name, expected 1 argument"
		return 1
	fi
	for i in ${VOXL_FAMILY_NAMES[@]}; do
		[[ "$i" == "$1" ]] && return 0
	done
	return 1
}



################################################################################
# BOARD NAMES
################################################################################

## names must be lowercase!!!!
export VOXL_BOARD_NAMES=(
	"voxl1"
	"voxl-flight"
	"rb5"
	"voxl2"
	"flight-core-v2"
	"voxl2-mini"
	"voxl2-mini-vrx")

export NUM_VOXL_BOARDS="${#VOXL_BOARD_NAMES[@]}"


voxl-print-board-table (){

	for i in `seq 1 $NUM_VOXL_BOARDS`; do
		echo "$i - ${VOXL_BOARD_NAMES[$i-1]}"
	done
}


voxl-board-code-to-name (){

	if [ "$#" -ne 1 ]; then
		>&2 echo "ERROR in voxl-board-code-to-name, expected 1 argument"
		return
	fi
	if [ "$1" -lt 1 ] || [ "$1" -gt ${NUM_VOXL_BOARDS} ]; then
		>&2 echo "ERROR in voxl-board-code-to-name, board must be between 1 and $NUM_VOXL_BOARDS"
		return
	fi
	echo "${VOXL_BOARD_NAMES[$1-1]}"
}

voxl-board-name-to-code (){

	if [ "$#" -ne 1 ]; then
		>&2 echo "ERROR in voxl-board-name-to-code, expected 1 argument"
		return
	fi

	for i in `seq 1 $NUM_VOXL_BOARDS`; do
		if [[ "${VOXL_BOARD_NAMES[$i-1]}" = "${1}" ]]; then
			echo $i
			return
		fi
	done

	echo "ERROR in voxl-board-name-to-code, unknown board name ${1}"
	echo "valid board names: ${VOXL_BOARD_NAMES[@]}"
}


voxl-is-valid-board-code (){
	if [ "$#" -ne 1 ]; then
		>&2 echo "ERROR in voxl-is-valid-board-code, expected 1 argument"
		return 1
	fi
	## make sure it's a digit
	if ! [[ "$1" =~ ^[[:digit:]]+$ ]]; then
		return 1
	fi
	## make sure that digit is in range
	if [ "$1" -lt 1 ] || [ "$1" -gt ${NUM_VOXL_BOARDS} ]; then
		return 1
	fi
	return 0
}

voxl-is-valid-board-name () {
	if [ "$#" -ne 1 ]; then
		>&2 echo "ERROR in voxl-is-valid-board-name, expected 1 argument"
		return 1
	fi
	for i in ${VOXL_BOARD_NAMES[@]}; do
		[[ "$i" == "$1" ]] && return 0
	done
	return 1
}





################################################################################
# TRANSMITTER NAMES
################################################################################

export VOXL_TRANSMITTER_NAMES=(
	"none" #0
	"spektrum" #1
	"unknown" #2
	"unknown" #3
	"unknown" #4
	"unknown" #5
	"tbs_crossfire" #6
	"elrs_beta_fpv" #7
	"elrs_m0184" #8
	"ghost" #9
	"elrs_m0193") #10

export NUM_TRANSMITTER_CONFIGS="${#VOXL_TRANSMITTER_NAMES[@]}"

voxl-transmitter-code-to-name (){

	if [ "$#" -ne 1 ]; then
		>&2 echo "ERROR in voxl-transmitter-code-to-name, expected 1 argument"
		return
	fi
	if [ "$1" == "0" ]; then
		echo "none"
		return
	fi
	if [ "$1" -lt 1 ] || [ "$1" -gt ${NUM_TRANSMITTER_CONFIGS} ]; then
		>&2 echo "ERROR in voxl-transmitter-code-to-name, transmitter number must be between 1 and $NUM_TRANSMITTER_CONFIGS"
		return
	fi
	echo "${VOXL_TRANSMITTER_NAMES[$1]}"
}


################################################################################
# MODEM NAMES
################################################################################

export VOXL_MODEM_NAMES=(
	"none" # 0
	"unknown" # 1
	"unknown" # 2
	"Microhard v2, No Radio (M0048-3)" # 3
	"LTE v2, 7610 NA (M0030)" # 4
	"LTE v2, 7607 EMEA EOL (M0030)" # 5
	"LTE v2, No Modem (M0030)" # 6
	"VOXL 2 5G Quectel (M0067/M0090-3-01)" # 7
	"LTE v2, 7611 NA (M0030)" # 8
	"VOXL 2 5G Telit (M0090-1-03)" # 9
	"Microhard v2, pMDDL2350 (M0048)" # 10
	"Microhard v2, pMDDL2450(M0048)" # 11
	"LTE v2, 7620 EMEA (M0030)" # 12
	"M0078-2 w/ Alpha WiFi Dongle, AC600" # 13
	"VOXL 5G Telit (M0090-1-03)" # 14
	"VOXL 2 5G Quectel (M0090-3-01)" # 15
	"M0078-2 w/ Alfa Networks WiFi Dongle, AC1200" # 16
	"M0078-2 w/ Alfa Networks WiFi Dongle, AC600 (modified)" # 17
	"Doodle RM-2450, 2.4GHz" # 18
	"Doodle RM-2025, Hex Band" # 19
	"M0130 RC7611 (Americas)" # 20
	"M0130 RC7620 (EMEA)" # 21
	"M0141 w/ Alfa Networks WiFi Dongle AWUS036EACS" # 22
	"Doodle Mini-OEM, RM-1700-22M3 (915MHz, 2.4GHz)" # 23
	"M0090-3-2 Sierra EM9291)" # 24
	"VTX (M0176 + M0175)" # 25
	"VTX (M0185)" # 26
	"Microhard, pMDDL1621 (M0059-1)" # 27
	"M0151 w/ Alfa Networks WiFi Dongle AWUS036EACS" # 28
	"Microhard v2, pMDDL 1800 (M0048)"  # 29
	"VRX (M0206, Mini Pini)"  # 30
	"VRX (M0206, SparkLAN)"  # 31
	"WaveMux" ) # 32

export NUM_MODEM_CONFIGS="${#VOXL_MODEM_NAMES[@]}"

voxl-modem-code-to-name (){

	if [ "$#" -ne 1 ]; then
		>&2 echo "ERROR in voxl-modem-code-to-name, expected 1 argument"
		return
	fi
	if [ "$1" -gt ${NUM_MODEM_CONFIGS} ]; then
		>&2 echo "ERROR in voxl-modem-code-to-name, modem number must be between 0 and $NUM_MODEM_CONFIGS"
		return
	fi
	echo "${VOXL_MODEM_NAMES[$1]}"
}

print-modem-options (){
	for i in "${!VOXL_MODEM_NAMES[@]}"; do
		echo "$i - ${VOXL_MODEM_NAMES[$i]}"
	done
}

################################################################################
# EXTRAS (addons)
################################################################################

## this is no longer a bitmask
export VOXL_EXTRAS_NAMES=(
	"none" # 0
	"CADDX Air Unit" # 1
	"USB-Boson" # 2
	"CADDX Vista + USB-Boson" # 3
	"HD-Zero" # 4
	"CADDX Vista" # 5
	"unknown" # 6
	"unknown" # 7
	"Lepton" # 8
	"none" # 9
	"none" # 10
	"none" # 11
	"HD Zero + Lepton" # 12
	"none" # 13
	"Walksnail + Lepton") # 14
	

export NUM_EXTRAS_CONFIGS="${#VOXL_EXTRAS_NAMES[@]}"

voxl-extras-code-to-name (){

	if [ "$#" -ne 1 ]; then
		>&2 echo "ERROR in voxl-extras-code-to-name, expected 1 argument"
		return
	fi
	if [ "$1" -gt ${NUM_EXTRAS_CONFIGS} ]; then
		>&2 echo "ERROR in voxl-extras-code-to-name, extras number must be between 0 and $NUM_EXTRAS_CONFIGS"
		return
	fi
	echo "${VOXL_EXTRAS_NAMES[$1]}"
}

print-extras-options (){
	for i in "${!VOXL_EXTRAS_NAMES[@]}"; do
		echo "$i - ${VOXL_EXTRAS_NAMES[$i]}"
	done
}


################################################################################
# General Functions
################################################################################

voxl-parse-and-export-sku-variables () {

	if [ "$#" -ne 1 ]; then
		>&2 echo "ERROR in parse-and-export-sku-variables, expected 1 argument"
		return 1
	fi


	## actually parse the SKU
	FRONT=$( echo "$1" | grep -Eo "^[A-Z]+-[A-Z][0-9]+-[0-9]+" )
	VOXL_FAMILY_CODE=$(  echo "$1" | grep -Eo "^[A-Z]+-[A-Z][0-9]+" )

	VOXL_BOARD_CODE=$(   echo "$1" | grep -Eo "^[A-Z]+-[A-Z][0-9]+-[0-9]+" | grep -Eo "[0-9]+$" )
	
	## ERROR checking
	if ! voxl-is-valid-family-code $VOXL_FAMILY_CODE ; then
		echo "ERROR bad family: $VOXL_FAMILY_CODE"
		return 1
	fi

	if [[ "$VOXL_FAMILY_CODE" != *"MVX-R0001"* ]]; then
		if [[ "$VOXL_FAMILY_CODE" != *"MCCA"* ]]; then

			VOXL_HW_VERSION=$(   echo "$1" | sed "s/$FRONT//g" | grep -Eo "[Vv][0-9]+" | grep -Eo "[0-9]+" )

			## parse modem number
			VOXL_MODEM_NUM=$(   echo "$1" | sed "s/$FRONT//g" | grep -Eo "[Mm][0-9]+" | grep -Eo "[0-9]+" )
			if [ "$VOXL_MODEM_NUM" == "" ]; then
				VOXL_MODEM_NUM="0"
			fi

			## parse transmitter number
			VOXL_TRANSMITTER_NUM=$(   echo "$1" | sed "s/$FRONT//g" | grep -Eo "[Tt][0-9]+" | grep -Eo "[0-9]+" )
			if [ "$VOXL_TRANSMITTER_NUM" == "" ]; then
				VOXL_TRANSMITTER_NUM="0"
			fi

			## parse misc number
			VOXL_EXTRAS_NUM=$(   echo "$1" | sed "s/$FRONT//g" | grep -Eo "[Xx][0-9]+" | grep -Eo "[0-9]+" )
			if [ "$VOXL_EXTRAS_NUM" == "" ]; then
				VOXL_EXTRAS_NUM="0"
			fi

			## parse experimental number
			VOXL_EXP_NUM=$(   echo "$1" | sed "s/$FRONT//g" | grep -Eo "[Ee][0-9]+" | grep -Eo "[0-9]+" )
			if [ "$VOXL_EXP_NUM" == "" ]; then
				VOXL_EXP_NUM=""
			fi

			## parse cam number including the custom mode
			VOXL_CAM_NUM=$(      echo "$1" | sed "s/$FRONT//g" | grep -Eo "[Cc][0-9]+" | grep -Eo "[0-9]+" )
			if [ "$VOXL_CAM_NUM" == "" ]; then
				if [[ "$1" == *"-CC"* ]]; then
					VOXL_CAM_NUM="C"
				fi
			fi

			## check if we got a new cam number
			if [ "$VOXL_CAM_NUM" == "" ]; then
				echo "ERROR empty cam number"
				return 1
			fi

			if ! voxl-is-valid-board-code $VOXL_BOARD_CODE ; then
				echo "ERROR bad board code: $VOXL_BOARD_CODE"
				return 1
			fi

			if [ "$VOXL_HW_VERSION" == "" ]; then
				echo "ERROR empty version number"
				return 1
			fi

		else
			VOXL_CAM_NUM=$(      echo "$1" | grep -Eo "[Cc][0-9]+" | grep -Eo "[0-9]+" )
			if [ "$VOXL_CAM_NUM" == "" ]; then
				if [[ "$1" == *"-CC"* ]]; then
					VOXL_CAM_NUM="C"
				else
					VOXL_CAM_NUM="0"
				fi
			fi

			VOXL_HW_VERSION="1"
			if [[ "$VOXL_FAMILY_CODE" == "MCCA-M0054" ]]; then
				VOXL_BOARD_CODE="4"
			elif [[ "$VOXL_FAMILY_CODE" == "MCCA-M0104" ]]; then
				VOXL_BOARD_CODE="6"
			else
				echo -e "${RED}[ERROR] in voxl-parse-and-export-sku-variables, unknown family $VOXL_FAMILY_CODE"
				return 1
			fi
		fi
	else
		VOXL_HW_VERSION=$(   echo "$1" | sed "s/$FRONT//g" | grep -Eo "[Vv][0-9]+" | grep -Eo "[0-9]+" )

		## parse modem number
		VOXL_MODEM_NUM=$(   echo "$1" | sed "s/$FRONT//g" | grep -Eo "[Mm][0-9]+" | grep -Eo "[0-9]+" )
		if [ "$VOXL_MODEM_NUM" == "" ]; then
			VOXL_MODEM_NUM="0"
		fi

		if ! voxl-is-valid-board-code $VOXL_BOARD_CODE ; then
			echo "ERROR bad board code: $VOXL_BOARD_CODE"
			return 1
		fi

		if [ "$VOXL_HW_VERSION" == "" ]; then
			echo "ERROR empty version number"
			return 1
		fi
	fi
	
	## export variables to environment for use in other scripts
	export VOXL_SKU
	export VOXL_HW_VERSION
	export VOXL_CAM_NUM
	export VOXL_EXP_NUM

	export VOXL_FAMILY_CODE
	VOXL_FAMILY_NAME=$(voxl-family-code-to-name $VOXL_FAMILY_CODE)
	export VOXL_FAMILY_NAME

	export VOXL_BOARD_CODE
	VOXL_BOARD_NAME=$(voxl-board-code-to-name $VOXL_BOARD_CODE)
	export VOXL_BOARD_NAME

	export VOXL_TRANSMITTER_NUM
	if [[ -n "$VOXL_TRANSMITTER_NUM" ]]; then
		VOXL_TRANSMITTER_NAME=$(voxl-transmitter-code-to-name $VOXL_TRANSMITTER_NUM)
	fi
	export VOXL_TRANSMITTER_NAME

	export VOXL_MODEM_NUM
	if [[ -n "$VOXL_MODEM_NUM" ]]; then
		VOXL_MODEM_NAME=$(voxl-modem-code-to-name $VOXL_MODEM_NUM)
	fi
	export VOXL_MODEM_NAME

	export VOXL_EXTRAS_NUM
	if [[ -n "$VOXL_EXTRAS_NUM" ]]; then
		VOXL_EXTRAS_NAME=$(voxl-extras-code-to-name $VOXL_EXTRAS_NUM)
	fi
	export VOXL_EXTRAS_NAME

}


voxl-print-sku-variables (){
	echo -e "family code:   $VOXL_FAMILY_CODE ($VOXL_FAMILY_NAME)"
	echo -e "compute board: $VOXL_BOARD_CODE ($VOXL_BOARD_NAME)"
	echo -e "hw version:    $VOXL_HW_VERSION"
	echo -e "cam config:    $VOXL_CAM_NUM"
	echo -e "modem config:  $VOXL_MODEM_NUM ($VOXL_MODEM_NAME)"
	echo -e "tx config:     $VOXL_TRANSMITTER_NUM ($VOXL_TRANSMITTER_NAME)"
	echo -e "extras config: $VOXL_EXTRAS_NUM ($VOXL_EXTRAS_NAME)"

	if [ "$VOXL_EXP_NUM" != "" ]; then
		echo -e "experimental config:  $VOXL_EXP_NUM"
	fi
	
	echo -e "SKU:           $VOXL_SKU"
}

voxl-print-sku-variables-json (){
    echo "{"
    echo "  \"family_code\": \"$VOXL_FAMILY_CODE\","
    echo "  \"family_name\": \"$VOXL_FAMILY_NAME\","
    echo "  \"compute_board_code\": \"$VOXL_BOARD_CODE\","
    echo "  \"compute_board_name\": \"$VOXL_BOARD_NAME\","
    echo "  \"hardware_version\": \"$VOXL_HW_VERSION\","
    echo "  \"camera_config\": \"$VOXL_CAM_NUM\","
    echo "  \"modem_config\": {"
    echo "    \"code\": \"$VOXL_MODEM_NUM\","
    echo "    \"name\": \"$VOXL_MODEM_NAME\""
    echo "  },"
    echo "  \"transmitter_config\": {"
    echo "    \"code\": \"$VOXL_TRANSMITTER_NUM\","
    echo "    \"name\": \"$VOXL_TRANSMITTER_NAME\""
    echo "  },"
    echo "  \"extras_config\": {"
    echo "    \"code\": \"$VOXL_EXTRAS_NUM\","
    echo "    \"name\": \"$VOXL_EXTRAS_NAME\""
    echo "  }"
	echo "}"
}


